import React from "react";
import "./style.scss";

export default class Modal extends React.Component {
  render() {
    return (
      <div onClick={this.props.isOpen} className={"my-modal"}>
        <div
          onClick={(event) => event.stopPropagation()}
          className="modal__content"
        >
          {this.props.content}
        </div>
      </div>
    );
  }
}
