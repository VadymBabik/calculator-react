import React from "react";
import "./style.scss";

export default function Navbar() {
  return (
    <nav>
      <div className="nav-wrapper blue darken-2">
        <a href="#" className="brand-logo">
          React
        </a>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li>
            <a href="/">Docs</a>
          </li>
          <li>
            <a href="/">Tutorial</a>
          </li>
          <li>
            <a href="/">Blog</a>
          </li>
          <li>
            <a href="/">Communitty</a>
          </li>
        </ul>
      </div>
    </nav>
  );
}
