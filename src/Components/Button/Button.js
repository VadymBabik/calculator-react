import React from "react";
// import "./style.scss";

export default class Button extends React.Component {
  render() {
    return (
      <a
        onClick={this.props.isOpen}
        className="center waves-effect  pink lighten-3 btn-large"
      >
        <i className="material-icons left">dialpad</i>Calculator
      </a>
    );
  }
}
