import React from "react";
import "./style.scss";

const INIT_STATE = {
  operand1: "",
  operand2: "",
  operator: null,
  result: null,
};

export default class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = INIT_STATE;
  }

  handelPushDigit = (digit) => {
    return () => {
      if (this.state.operator) {
        this.setState({
          operand2: this.state.operand2 + digit,
        });
      } else {
        this.setState({
          operand1: this.state.operand1 + digit,
        });
      }
    };
  };

  handleEqual = () => {
    const { operand1, operand2, operator } = this.state;

    const res =
      operator === "+" ? +operand1 + +operand2 : +operand1 - +operand2;

    this.setState({
      result: res,
    });
  };

  render() {
    const { operand1, operand2, operator, result } = this.state;
    return (
      <section>
        <div className="wrapper">
          <div className="header">
            <span className={"title"}>Calculator</span>
            <span className={"close"} onClick={this.props.isOpen}>
              <i className="material-icons">close</i>
            </span>
          </div>
          <div className="body">
            <div className="result">
              {result || (operator ? operand2 : operand1)}
            </div>
          </div>
          <div className="btnContainer">
            <div className="digitalContainer">
              <a
                onClick={this.handelPushDigit("1")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                1
              </a>
              <a
                onClick={this.handelPushDigit("2")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                2
              </a>
              <a
                onClick={this.handelPushDigit("3")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                3
              </a>
              <a
                onClick={this.handelPushDigit("4")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                4
              </a>
              <a
                onClick={this.handelPushDigit("5")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                5
              </a>
              <a
                onClick={this.handelPushDigit("6")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                6
              </a>
              <a
                onClick={this.handelPushDigit("7")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                7
              </a>
              <a
                onClick={this.handelPushDigit("8")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                8
              </a>
              <a
                onClick={this.handelPushDigit("9")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                9
              </a>
              <a
                onClick={this.handelPushDigit("0")}
                disabled={result}
                className="waves-effect btn-large waves-light btn"
              >
                0
              </a>
            </div>
            <div className="actionsContainer">
              <a
                onClick={() => {
                  this.setState(INIT_STATE);
                }}
                disabled={!operand1}
                className="waves-effect btn-large red lighten-3 btn"
              >
                C
              </a>
              <a
                onClick={() => {
                  this.setState({
                    operator: "+",
                  });
                }}
                disabled={!operand1 || operator}
                className="waves-effect btn-large red lighten-3 btn"
              >
                +
              </a>
              <a
                onClick={() => {
                  this.setState({
                    operator: "-",
                  });
                }}
                disabled={!operand1 || operator}
                className="waves-effect btn-large red lighten-3 btn"
              >
                -
              </a>
              <a
                onClick={this.handleEqual}
                disabled={!operand2 || result}
                className="waves-effect btn-large red lighten-3 btn"
              >
                =
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
