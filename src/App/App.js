import React from "react";
import "./style.scss";
import "materialize-css/dist/css/materialize.min.css";
import Navbar from "../Components/Navbar/Navbar";
import Button from "../Components/Button/Button";
import Calculator from "../Components/Calculator/Calculator";
import Modal from "../Components/Modal/Modal";

export default class App extends React.Component {
  state = {
    calc: false,
  };

  calcIsOpen = () => {
    this.setState({
      calc: !this.state.calc,
    });
  };
  render() {
    return (
      <div className="App">
        <header>
          <Navbar />
        </header>
        <main>
          <div className="wrapper-button">
            <Button isOpen={this.calcIsOpen} />
          </div>
          {this.state.calc && (
            <Modal
              isOpen={this.calcIsOpen}
              content={<Calculator isOpen={this.calcIsOpen} />}
            />
          )}
        </main>
        <footer></footer>
      </div>
    );
  }
}
